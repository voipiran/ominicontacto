# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2019, omnileads
# This file is distributed under the same license as the OMniLeads package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: OMniLeads \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-20 15:46-0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.6.0\n"

# 5742112fa46c4f54829a20a0eae43bda
#: ../../campaigns_manual.rst:5
msgid "Campañas manuales"
msgstr "Manual Campaigns"

# aae90a9364d14a349689884bbb718d99
#: ../../campaigns_manual.rst:7
msgid ""
"Para crear una nueva campaña manual se debe ingresar al punto de menú "
"*Manual Campaigns -> New  Campaign*. El proceso de creación consta de un "
"wizard de dos pantallas."
msgstr ""
"To create a new manual campaign you must enter the menu item Manual "
"Campaigns -> New Campaign. "

# 1f0a6ab330b149709d66e57ce1335727
#: ../../campaigns_manual.rst:10
msgid ""
"La primera pantalla nos invita a indicar una serie de parámtros de la "
"campaña, como lo indica la figura 1."
msgstr ""
"The first stage invites us to indicate a series of campaign parameters, as"
" indicated in Figure 1."

# bfa9430acbf6476e9d2c3be1847d78bb
#: ../../campaigns_manual.rst:14
msgid "*Figure 1: Campaigns parameters*"
msgstr ""

# c84093dc9f834fe39a9d4efd54e31bbe
#: ../../campaigns_manual.rst:17
msgid "**Name:** nombre de la campaña"
msgstr "**Name:** Campaign name"

# 425c99ff621541a9810190e95c68a3b0
#: ../../campaigns_manual.rst:18
msgid ""
"**Contact database:** se utiliza para desplegar datos extras al teléfono "
"a la hora de ejecutar una llamada a un contacto de la campaña."
msgstr ""
"**Contact database:** it is used to display extra data to the phone "
"number, when executing a call to a campaign contact."

# 3515a49e7a894bb8b5833af32aa2f4bb
#: ../../campaigns_manual.rst:19
msgid ""
"**External system:** sistema de gestión externo que ejecutaría \"click to"
" call\" sobre la campaña."
msgstr ""
"**External system:** Selects the external CRM that will execute the requests "
"against OML (External System)"

# 2955646a0218488f943110c9b131361c
#: ../../campaigns_manual.rst:20
msgid ""
"**ID on external system:**  ID de la campaña en el sistema de gestión "
"externo."
msgstr ""
"**ID on external system:** campaign ID in the external CRM"

# 9a61abd388a04f5c84cd0005041edc03
#: ../../campaigns_manual.rst:21
msgid ""
"**Enable recordings:** habilitar la grabación de todas las llamadas que "
"se cursen por la campaña."
msgstr ""
"**Enable recordings:** enable the recording of all calls that are "
"placed through the campaign."

# f395a62db8c944a8ba32aeec719686a0
#: ../../campaigns_manual.rst:22
msgid ""
"**Scope:** se define como la cantidad de gestiones positivas que se "
"esperan para la campaña. En la supervisión de la campaña se muestra en "
"tiempo real el porcentaje de avence de la campaña respecto al objetivo "
"definido."
msgstr ""
"**Scope:** it is defined as the amount of *engaged dispositions* expected"
" for the campaign. In the campaign monitoring, the percentage of the "
"campaign's arrival with respect to the defined objective is shown in real"
" time."

# c57b0d07a12d47d4a4b5a61b56cf706d
#: ../../campaigns_manual.rst:25
msgid ""
"El tema de la base de contactos en las campañas manuales (y también "
"entrantes) plantea un escenario flexible, ya que es Opcional el hecho de "
"asignar una base de contactos a este tipo de campañas. En este caso, la "
"base de contactos es utilizada si deseamos que cada vez que un agente "
"marca un teléfono que corresponde con un contacto de la base, se puedan "
"recuperar los datos (columnas extras al teléfono) del mismo. Además el "
"hecho de trabajar con una base de contactos en una campaña manual permite"
" calificar cada contacto llamado."
msgstr ""
"Regarding the contact base in manual (and also incoming) campaigns, it "
"poses a flexible scenario, since it is optional to assign a contact "
"database to this kind of campaign. In this case, the contact database is "
"used if we want that each time an agent dials a telephone that "
"corresponds to a contact in the database, the data (extra columns to the "
"telephone) can be retrieved from it. In addition to working with a "
"contact base in a manual campaign allows you to rate each contact called."

# 1d0a5e31c66140cab24b33d9c082aab2
#: ../../campaigns_manual.rst:30
msgid ""
"En la segunda pantalla se deben asignar las calificaciones que se "
"requieran para que los agentes puedan clasificar cada llamada realizada "
"al contacto. Como se puede apreciar en la figura 2, en nuestro ejemplo "
"manejamos dos calificaciones que disparan dos formularios diferentes."
msgstr ""
"On the second screen, the dispostions required for agents to classify "
"each call made to the contact must be assigned.As can be seen in Figure "
"2, in our example we handle two ratings that trigger two different forms."

# 8ea6c76d0db144abac74b4b92bc66a9e
#: ../../campaigns_manual.rst:35
msgid "*Figure 2: Call dispositions*"
msgstr ""

# 86470c5c625741578929ffc174f6fad8
#: ../../campaigns_manual.rst:37
msgid ""
"En los siguientes pasos se pueden añadir supervisores y agentes a nuestra"
" campaña."
msgstr ""
"In the following steps you can add supervisors and agents to our campaign"

# f361676d937044b19dea5a81bc5b11d2
#: ../../campaigns_manual.rst:41
msgid "*Figure 3: agent assignment*"
msgstr ""

# 13103b53dd37463ab2f08e6dd3f00b09
#: ../../campaigns_manual.rst:43
msgid ""
"Finalmente contamos con nuestra nueva campaña manual. cuando un agente "
"asignado a la misma realice un login a la plataforma y comience a marcar "
"llamadas desde su webphone, el sistema le permitirá seleccionar la "
"campaña sobre la cual va a asignar cada llamada manual generada desde el "
"webphone, tal como se expone en la figura 4."
msgstr ""
"Finally, we have our new manual campaign, so when an agent assigned to it"
" makes a login to the platform and starts dialing calls from your "
"webphone, the system will allow you to select the campaign on which you "
"will assign each generated manual call from the webphone, as shown in "
"figure 4."

# 9e39939230ab41289ae4c182be0f98c5
#: ../../campaigns_manual.rst:49
msgid "*Figure 4: Manual call camp selection*"
msgstr ""

# 26f863cde2c0467fb6806d06f8f99037
#: ../../campaigns_manual.rst:52
msgid ""
"Cuando un agente se encuentra online y marca un teléfono correspondiente "
"a un contacto de la base de la campaña, el contacto cuyo teléfono "
"coincide con el teléfono marcado por el agente es desplegado como "
"contacto a seleccionar y así desplegar sus datos en la pantalla de "
"agente, también se permite generar un nuevo contacto. Entonces el agente "
"puede o bien confirmar que la llamada se dispara hacia el contacto "
"listado o sino también crear un nuevo contacto y marcarlo. Los datos "
"(extras al teléfono) del contacto son desplegados en la pantalla de "
"agente"
msgstr ""
"When an agent is online and dials a phone corresponding to a contact at "
"the campaign database, the contact whose phone matches the phone dialed "
"by the agent is displayed as a contact to select and thus display their "
"data on the agent screen, it is also allowed to generate a new contact. "
"Then the agent can either confirm that the call is triggered towards the "
"listed contact or also create a new contact and dial it. The contact "
"information (extra phone) is displayed on the agent screen. "

# 2a2c17188a0043f0a7edfdc41b45f464
#: ../../campaigns_manual.rst:59
msgid "*Figure 5: Contact selection*"
msgstr ""

# 8b4deaf3173c4a079c167b7871d4913d
#: ../../campaigns_manual.rst:61
msgid ""
"Si se selecciona llamar al contacto listado, entonces los datos del mismo"
" son desplegados en pantalla, como lo expone la figura 6."
msgstr ""
"If you select to call the contact listed, then the data is displayed on "
"the screen, as shown in Figure 6."

# 881687fc7485490d827f2cb78a9909b5
#: ../../campaigns_manual.rst:65
msgid "*Figure 6: Contact data*"
msgstr ""

# 5e3d69818f494acea1a57d48941a9e38
#: ../../campaigns_manual.rst:67
msgid ""
"De esta manera el agente puede asignar una calificación sobre el contacto"
" llamado; figura 7."
msgstr ""
"In this way the agent can assign a rating on the called contact; figure 7."

# c28be31c4d7840e4bb51e617ab1ffc00
#: ../../campaigns_manual.rst:71
msgid "*Figure 7: call disposition for contact*"
msgstr ""

# d1b4d66b784d426ea12762a1badab549
#: ../../campaigns_manual.rst:74
msgid ""
"Por otro lado, si el teléfono marcado no corresponde a ningún contacto de"
" la base entonces el sitema permite al agente buscar el contacto en la "
"base o generar un nuevo contacto. En caso de tratarse de una campaña sin "
"base de contactos, entonces cada llamado que realice un agente implica "
"que se genere el contacto asociado a la llamada marcada (figura 8 y 9)."
msgstr ""
"On the other hand, if the dialed telephone does not correspond to any "
"contact of the database, then the system allows the agent to search for "
"the contact in the database or generate a new contact. In the case of a "
"campaign without a contact database, then each call made by an agent "
"implies that the contact associated to the dialed call is generated "
"(figures 5 and 6)."

# ad6c1b5eea6e4f9b849fd30a8d660a82
#: ../../campaigns_manual.rst:79
msgid "*Figure 8: new contact add to campaign database 1*"
msgstr ""

# f8c405881bc743418e7133322d64be4c
#: ../../campaigns_manual.rst:84
msgid "*Figure 9: new contact add to campaign database 2*"
msgstr ""

# 841ff7ad50934fa2a08d386586df6b2a
#: ../../campaigns_manual.rst:86
msgid ""
"Entonces al momento de marcar a un número que no devuelva un contacto, el"
" agente pasará por una vista en la que primero el agente debe añadir el "
"contacto como un registro de la base de la campaña para luego marcar. "
"Finalmente se despliegan el nuevo contacto y la opción de clasificar la "
"llamada con alguna calificación (figura 10)."
msgstr ""
"Then at the time of dialing a number that does not return a contact, the "
"agent will go through a view in which the agent must first add the "
"contact as a record of the campaign base and then dial. Finally, the new "
"contact and the option to classify the call with some qualification are "
"displayed (figure 10)."

# 39ce8cc7d4b34e2586b77e588f23509b
#: ../../campaigns_manual.rst:92
msgid "*Figure 10: new contact called*"
msgstr ""

# 2f21d281a97f4a448616d489bc006802
#: ../../campaigns_manual.rst:94
msgid "**Campaña con base de datos Multinum**"
msgstr "**Multinum contact database**"

# 72d8bec53a75409c971fb3c2428b41b3
#: ../../campaigns_manual.rst:96
msgid ""
"Como sabemos, OMniLeads admite que cada contacto de una base posea \"n\" "
"números de teléfono de contacto, de manera tal que si el contacto no es "
"encontrado en su número principal (el primero de nuestro archivo CSV de "
"base), pueda ser contactado a los demás números. En este caso, cada "
"número de teléfono (que indicamos en la carga de la base) se genera como "
"un link dentro de los datos del contacto presentados sobre la pantalla de"
" agente. Al hacer click sobre dicho link, se dispara una llamada hacia "
"ekl número de teléfono extra del contacto. En la figura 11 se muestra "
"dicho escenario."
msgstr ""
"As we know, OMniLeads admits that each contact of a database has *n* "
"telephone numbers, so that if the contact is not found in its main number"
" (the first of our database CSV file), it can be contacted at the other "
"phone numbers. In this case, each telephone number (which we indicate in "
"the database load) is generated as a link within the contact data "
"presented on the agent screen. Clicking on that link triggers a call to "
"the extra telephone number of the contact. Figure 11 shows this scenario."

# 73bcae274bbc495794b8d2ba873f3baf
#: ../../campaigns_manual.rst:103
msgid "*Figure 11: Multinum contact database*"
msgstr ""

# c7536dfd47c84ee4a233fd5556baff15
#: ../../campaigns_manual.rst:105
msgid ""
"Por lo tanto, el agente puede intentar contactar a todos los números "
"disponibles como \"link\" en la ficha del contacto, hasta finalmente "
"calificar y pasar a uno nuevo."
msgstr ""
"Therefore, the agent can try to contact all available numbers as a *link*"
" in the contact form, until finally selecting a disposition and moving to"
" a new one."

#~ msgid "Manual Call Campaign"
#~ msgstr ""

#~ msgid ""
#~ "**Campaign form:** campo de selección "
#~ "del formulario que se desplegará cada"
#~ " vez que un agente califique un "
#~ "contacto con la calificación \"de "
#~ "gestión\"."
#~ msgstr ""

#~ msgid ""
#~ "**Nota 1:** El tema de la base "
#~ "de contactos en las campañas manuales"
#~ " (y también entrantes) plantea un "
#~ "escenario flexible, ya que es Opcional"
#~ " el asignar una base de contactos "
#~ "a este tipo de campañas. En este"
#~ " caso, la base de contactos es "
#~ "utilizada si deseamos que cada vez "
#~ "que un contacto marcado por un "
#~ "agente, en base a su número de "
#~ "teléfono poder recuperar los datos "
#~ "(columnas extras al teléfono de la "
#~ "base) del mismo."
#~ msgstr ""

#~ msgid ""
#~ "En la segunda pantalla se deben "
#~ "asignar las calificaciones que se "
#~ "requieran para que los agentes úedan "
#~ "clasificar cada llamada de contacto."
#~ msgstr ""

#~ msgid ""
#~ "Una vez lista la campaña solo "
#~ "resta asignar a los agentes que "
#~ "podrán trabajar en la misma, opteniendo"
#~ " contactos para marcar. En la figura"
#~ " 3 y 4 se ejemplifica una "
#~ "asignación de agentes a una campaña."
#~ msgstr ""

#~ msgid ""
#~ "Finalmente cuando un agente asignado a"
#~ " la campaña realice un login a "
#~ "la plataforma, debería disponer de la"
#~ " campaña manual a la hora de "
#~ "seleccionar a qué campaña asignar cada"
#~ " llamada manual desde el webphone, "
#~ "tal como se expone en la figura"
#~ " 5."
#~ msgstr ""

#~ msgid "*Figure 4: Preview agents view*"
#~ msgstr ""

#~ msgid ""
#~ "Cuando un agente se encuentra online "
#~ "y marca un teléfono correspondiente a"
#~ " un contacto de la base de la"
#~ " campaña, el contacto cuyo teléfono "
#~ "coincide con por el marcado por el"
#~ " agente es desplegado como opción a"
#~ " seleccionar en esta pantalla, también "
#~ "se permite generar un nuevo contacto."
#~ " Entonces el agente puede o bien "
#~ "confirmar que la llamada se dispara "
#~ "hacia el contacto listado o sino "
#~ "también crear un nuevo contacto y "
#~ "marcarlo. Los datos (extras al teléfono)"
#~ " del contacto son desplegados en la"
#~ " pantalla de agente"
#~ msgstr ""

#~ msgid "*Figure 5: agent assignment*"
#~ msgstr ""

#~ msgid "*Figure 6: agent assignment*"
#~ msgstr ""

#~ msgid "*Figure 7: call disposition assignment*"
#~ msgstr ""

#~ msgid "*Figure 8: new contact add to campaign database screen 1*"
#~ msgstr ""

#~ msgid "*Figure 9: new contact add to campaign database screen 2*"
#~ msgstr ""

#~ msgid ""
#~ "Entonces al momento de marcar a un"
#~ " número que no devuelva un contacto."
#~ " el agente pasará por una vista "
#~ "en la que primero el agente debe"
#~ " añadir el contacto como un registro"
#~ " de la base de la campaña para"
#~ " luego marcar. Finalmente se despliegan "
#~ "el nuevo contacto y la opción de"
#~ " clasificar la llamada con alguna "
#~ "calificación (figura 8)."
#~ msgstr ""

# b8cb5a35f0b040c2b8926a963280ea13
#~ msgid ""
#~ "**Contact database:** se utiliza para "
#~ "desplegar datos a partir del número "
#~ "de teléfono origen que realiza la "
#~ "llamada entrante al sistema."
#~ msgstr ""

# f318bba6cc6242d1b2316ebd8a47df3b
#~ msgid "**External URL:** URL a disparar cada vez que el agente lo indique."
#~ msgstr ""

# bc44c0dfac14493ca3c3b682db7c6ff5
#~ msgid ""
#~ "**Enable recordings:** habilitar que todas "
#~ "las llamadas de la campaña sean "
#~ "grabadas."
#~ msgstr ""

# 220afc4646c24071ad99f949103765b2
#~ msgid ""
#~ "**Nota 1:** El tema de la base "
#~ "de contactos en las campañas manuales"
#~ " (y también entrantes) plantea un "
#~ "escenario flexible, ya que es Opcional"
#~ " el asignar una base de contactos "
#~ "a este tipo de campañas. En este"
#~ " caso, la base de contactos es "
#~ "utilizada si deseamos que cada vez "
#~ "que un agente marca un teléfono "
#~ "que corresponde con un contacto de "
#~ "la base, se pueda recuperar los "
#~ "datos (columnas extras al teléfono de"
#~ " la base) del mismo y además "
#~ "poder calificar el contacto en la "
#~ "campaña.."
#~ msgstr ""

# 52b6c27f5c12469496fdb156b1db8a0d
#~ msgid ""
#~ "Por otro lado, si el teléfono "
#~ "marcado no corresponde a ningún contacto"
#~ " de la base entonces el sitema "
#~ "permite al agente buscar el contacto "
#~ "en la base o generar un nuevo "
#~ "contacto. En caso de tratarse de "
#~ "una campaña sin base de contactos, "
#~ "entonces cada llamado que realice un "
#~ "agente implica que se genere el "
#~ "contacto asociado a la llamada marcada"
#~ " (figura 5 y 6)."
#~ msgstr ""

# 0aef753b186b42c1b950c9fe295ad1ee
#~ msgid ""
#~ "Como sabemos, OMniLeads admite que cada"
#~ " contacto de una base posea \"n\" "
#~ "números de teléfono de contacto, de "
#~ "manera tal que si el contacto no"
#~ " es encontrado en su número principal"
#~ " (el primero de nuestro archivo CSV"
#~ " de base), pueda ser contactado a "
#~ "los demás números. En este caso, "
#~ "cada número de teléfono (que indicamos"
#~ " en la carga de la base) se "
#~ "genera como un link dentro de los"
#~ " datos del contacto presentados sobre "
#~ "la pantalla de agente. Al hacer "
#~ "click sobre dicho link, se dispara "
#~ "una llamada hacia ekl número de "
#~ "teléfono extra del contacto. En la "
#~ "figura 7 se muestra dicho escenario."
#~ msgstr ""
